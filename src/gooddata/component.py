import csv
# import json
import logging
import os
import re
import sys
from gooddata.client import ValidationClient
from kbc.env_handler import KBCEnvHandler


KEY_USERNAME = 'username'
KEY_PASSWORD = '#password'
KEY_PROJECTID = 'projectId'
KEY_METRICS = 'validationMetrics'
KEY_CUSTOMDOMAIN = 'customDomain'
KEY_GDURL = 'gooddataUrl'

MANDATORY_PARAMETERS = [KEY_USERNAME, KEY_PASSWORD, KEY_PROJECTID, KEY_METRICS]

SUPPORTED_METRICS = ["pdm::pdm_vs_dwh", "pdm::pk_fk_consistency", "pdm::elem_validation",
                     "invalid_objects", "pdm::transitivity", "metric_filter", "ldm"]

FIELDS_VALIDATION = ['validation_metric', 'category', 'level', 'message']


class ValidationComponent(KBCEnvHandler):

    def __init__(self):

        KBCEnvHandler.__init__(self, MANDATORY_PARAMETERS)
        self.validate_config(MANDATORY_PARAMETERS)

        self.paramUsername = self.cfg_params[KEY_USERNAME]
        self.paramPassword = self.cfg_params[KEY_PASSWORD]
        self.paramProjectId = self.cfg_params[KEY_PROJECTID]
        self.paramMetrics = self.cfg_params[KEY_METRICS]
        self.paramCustomDomain = self.cfg_params[KEY_CUSTOMDOMAIN]
        self.paramGooddataUrl = self.image_params[KEY_GDURL]

        self._processAndValidateParameters()

        self.client = ValidationClient(username=self.paramUsername,
                                       password=self.paramPassword,
                                       projectId=self.paramProjectId,
                                       baseGoodDataUrl=self.paramGooddataUrl)

        self.writer = csv.DictWriter(open(os.path.join(self.tables_out_path, 'validation.csv'), 'w'),
                                     fieldnames=FIELDS_VALIDATION, extrasaction='ignore', restval='',
                                     quotechar='\"', quoting=csv.QUOTE_ALL)
        self.writer.writeheader()

    def _processAndValidateParameters(self):

        custDomain = re.sub(r'\s', '', self.paramCustomDomain)

        if custDomain != '':

            rxgString = r'https://.*\.gooddata\.com/*'
            rgxCheck = re.fullmatch(rxgString, custDomain)

            if rgxCheck is None:

                logging.error("%s is not a valid GoodData domain." %
                              custDomain)
                sys.exit(1)

            else:

                self.paramGooddataUrl = custDomain

        logging.info("Using domain %s." % self.paramGooddataUrl)

        for metric in self.paramMetrics:

            if metric not in SUPPORTED_METRICS:

                logging.error("Metric %s is not supported. Please choose from: %s" % (
                    metric, SUPPORTED_METRICS))
                sys.exit(1)

    def run(self):

        reqValidationSc, reqValidationJs = self.client.triggerValidation(
            self.paramMetrics)

        if reqValidationSc == 201:

            integrationId = reqValidationJs['asyncTask']['link']['poll'].split(
                '/')[-1]

        else:

            logging.error("There was an error triggering the validation task.")
            logging.error("Received: %s - %s." %
                          (reqValidationSc, reqValidationJs))
            sys.exit(1)

        validJs = self.client.checkValidationStatus(
            integrationId)['projectValidateResult']['results']

        for result in validJs:

            if result['body']['error'] == 0:

                continue

            else:

                validationMetric = result['from']
                logMessages = result['body']['log']

                for message in logMessages:

                    msg = message['msg']
                    level = message['level']
                    category = message['ecat']
                    pars = message['pars']
                    parameters = []

                    for p in pars:

                        concatPar = ''

                        if 'object' in p:

                            name = p['object']['name']
                            obId = p['object']['id']
                            oUri = p['object']['uri']

                            concatPar = f'{name} ({obId} - {oUri})'

                            parameters += [concatPar]

                        elif 'common' in p:

                            parameters += [p['common']]

                        elif 'sli_el' in p:

                            parameters += [','.join(p['sli_el']['vals'])]

                    self.writer.writerow({'validation_metric': validationMetric,
                                          'category': category,
                                          'level': level,
                                          'message': msg % tuple(parameters)})
