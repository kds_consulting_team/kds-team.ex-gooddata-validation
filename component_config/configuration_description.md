The component accepts following parameters:

- login - login to GoodData, must have admin privileges,
- password - password associated with login,
- project ID - GoodData project ID,
- metrics - a list of validation metrics,
- custom domain (opt.) - if whitelabelled domain is used, specify it.