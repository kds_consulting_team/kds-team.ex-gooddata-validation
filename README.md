# GoodData Validation Extractor

The component allows to download validation report from GoodData project. The validation may take several minutes.

## Configuration

A sample of the configuration can be found in the [component's repository](https://bitbucket.org/kds_consulting_team/kds-team.ex-gooddata-validation/src/master/component_config/sample-config/).

### Parameters

The component accepts following parameters:

- login - login to GoodData, must have admin privileges,
- password - password associated with login,
- project ID - GoodData project ID,
- metrics - a list of validation metrics,
- custom domain (opt.) - if whitelabelled domain is used, specify it.

## Output

Output is a table with validation errors for the project.